# arrayBufer 二进制数组

1. 基本的二进制对象是 ArrayBuffer —— 对固定长度的连续内存空间的引用
2. 要访问单个字节，需要另一个“视图”对象
3. 视图对象本身并不存储任何东西。它是一副“眼镜”
   1. Uint8Array ,Uint16Array ,Uint32Array ,Float64Array (8B)
4. 没有名为 TypedArray 的构造器，它只是表示 ArrayBuffer 上的视图之一的通用总称术语
5. TypedArray 具有常规的 Array 方法
   1. 遍历（iterate），map，slice，find 和 reduce 等
   2. 没有 splice, 无 concat 方法
   3. arr.set(fromArr, [offset]) 从 offset（默认为 0）开始，将 fromArr 中的所有元素复制到 arr
   4. arr.subarray([begin, end]) 创建一个从 begin 到 end（不包括）相同类型的新视图; 似 slice 但不复制任何内容, 只是创建一个新视图
6. DataView 是在 ArrayBuffer 上的一种特殊的超灵活“未类型化”视图。它允许以任何格式访问任何偏移量（offset）的数据
   1. 使用 .getUint8(i) 或 .getUint16(i) 之类的方法访问数据

```js
let buffer = new ArrayBuffer(16) // 创建一个长度为 16 的 buffer

let view = new Uint32Array(buffer) // 将 buffer 视为一个 32 位整数的序列

alert(Uint32Array.BYTES_PER_ELEMENT) // 每个整数 4 个字节

alert(view.length) // 4，它存储了 4 个整数
alert(view.byteLength) // 16，字节中的大小

// 让我们写入一个值
view[0] = 123456

// 遍历值
for (let num of view) {
  alert(num) // 123456，然后 0，0，0（一共 4 个值）
}



// 4 个字节的二进制数组，每个都是最大值 255
let buffer = new Uint8Array([255, 255, 255, 255]).buffer

let dataView = new DataView(buffer)

// 在偏移量为 0 处获取 8 位数字
alert(dataView.getUint8(0)) // 255

// 现在在偏移量为 0 处获取 16 位数字，它由 2 个字节组成，一起解析为 65535
alert(dataView.getUint16(0)) // 65535（最大的 16 位无符号整数）

// 在偏移量为 0 处获取 32 位数字
alert(dataView.getUint32(0)) // 4294967295（最大的 32 位无符号整数）

dataView.setUint32(0, 0) // 将 4 个字节的数字设为 0，即将所有字节都设为 0
```

## TextDecoder 和 TextEncoder

1. 内建的 TextDecoder 对象在给定缓冲区（buffer）和编码格式（encoding）的情况下，允许将值读取为实际的 JavaScript 字符串。
